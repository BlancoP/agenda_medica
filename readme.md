Contenido: Contar con al menos 4 páginas HTML, con temáticas de libre elección. Si el diseño es un estilo de página única (One Page), se requiere que contenga al menos 4 secciones.

Etiquetas semánticas: Utilizar etiquetas semánticas para estructurar el contenido del sitio.

Formulario: Incluir un formulario de contacto con validación mediante JavaScript. Como opción adicional, el formulario puede enviar un correo electrónico utilizando un servicio externo de correo.

Diseño estético: Incorporar al menos un Iframe, íconos de FontAwesome, Flaticon o similar, y fuentes locales, de Google Fonts u otro similar.

Diseño responsive: Incluir al menos una página completamente adaptable a tres tamaños de dispositivos diferentes (Mobile, Tablet y Escritorio). Esto garantizará que tu proyecto se vea correctamente en diversos dispositivos, brindando una experiencia óptima a los usuarios.

Dinamismo visual: Introducir al menos una animación, transformación o transición en algún punto del sitio web (deberás indicar dónde se implementó la animación). Pueden utilizar librerías externas si lo desean.

Maquetación: Utilizar una estructura HTML diseñada con Flexbox y/o Grid.

API: Realizar una solicitud a una API Rest desde JavaScript. Debes indicar la API que usaste al momento de presentar el TPO.

Framework de CSS: En caso de querer usar Bootstrap (no obligatorio), solo podrán emplearlo en la página del formulario del sitio. No se admitirán proyectos desarrollados enteramente con Frameworks de CSS (como Bootstrap, Materialize, Tailwind, etc.) ni se podrán emplear estilos obtenidos de estos frameworks. Si estas pautas no se respetan, el trabajo práctico será considerado no válido.

Publicación y GIT: La página web debe ser cargada en un servidor en línea para que el Instructor pueda navegar por ella. Pueden utilizar GitHub Pages, Netlify u otras opciones similares. Nota: El TPO es un trabajo en equipo. Deberán sincronizar el código del proyecto en un repositorio en GitHub (u otra plataforma similar) y enviar el enlace del repositorio al Instructor al momento de entregar el proyecto. Este requisito es obligatorio. Para más información sobre GIT, consulta el material en el Aula Virtual bajo la sección "GIT".

Diseño Integral del Proyecto: Además de los aspectos técnicos previamente mencionados, se evaluará la parte de diseño del proyecto. Esto incluye: maquetación del sitio, alineación, orden y márgenes entre elementos, elección de tipografía acorde a la temática, uso de fotografías de calidad y optimizadas para la web, selección de paleta de colores adecuada, uso de logo e íconos, y favicon. Para asegurarte de cumplir con los criterios de diseño, te recomendamos revisar el material disponible en el Aula Virtual. Pueden encontrar un Seminario de Diseño Web en el enlace:  . Además, se proporcionará material adicional sobre diseño en el Aula Virtual bajo la sección "Diseño", que estará disponible después de la Unidad de Bootstrap.

Documentación: Incluir en la entrega final del proyecto web un archivo de documentación preliminar en formato PDF. Esta documentación deberá contener la planificación, organización, objetivo y descripción del proyecto, detallando cómo se han abordado los diferentes aspectos del desarrollo. Las pautas y especificaciones detalladas para este punto serán proporcionadas en un documento aparte para asegurar una presentación consistente y adecuada de la documentación.